#!/bin/bash

# Terminate already running picom instances
killall -q picom

# Wait until the process have been shut down
while pgrep -u $UID -x picom >/dev/null; do sleep 1; done

# Start Picom in background...
picom --experimental-backends -b

echo "Picom running..."
