-- vim: ai cindent syntax=lua
-- conky config
-- Author : Gerry Agbobada <gagbobada+git@gmail.com>

conky.config = {
    background = false,
    update_interval = 1,
    double_buffer = true,
    no_buffers = true,
    imlib_cache_size = 10,

    -- Window specifications --
    gap_x = 15,
    gap_y = 40,
    minimum_width = 268,
    -- minimum_height = 1220,
    maximum_width = 300,
    alignment = 'br',
    own_window = true,
    own_window_class = 'override',
    own_window_type = 'normal',
    own_window_transparent = false,
    own_window_argb_visual = true,
    own_window_argb_value = 40,
    own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager,below',
    -- own_window_type = 'normal',
    -- own_window_transparent = false,
    -- own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    -- own_window_argb_visual = true,
    -- own_window_argb_value = 0,
    border_inner_margin = 0,
    border_outer_margin = 0,

    -- Graphics settings --
    draw_shades = false,
    default_shade_color = '#AAAAAA',
    draw_outline = false,
    default_outline_color = '#AAAAAA',
    draw_borders = false,
    draw_graph_borders = false,
    -- default_graph_size = 26 80,
    show_graph_scale = false,
    show_graph_range = false,

    -- Text settings --
    use_xft = true,
    xftalpha = 0.9,
    font = 'Noto Sans:size=10',
    text_buffer_size = 256,
    override_utf8_locale = true,

    -- Useful shortenings --
    short_units = true,
    pad_percents =  2,
    top_name_width = 30,

    -- Sampling
    cpu_avg_samples = 2,
    net_avg_samples = 2,

    -- Color scheme --
    default_color = '#fce8c3',

    color0  = '#1C1B19',
    color1  = '#EF2F27',
    color2  = '#519F50',
    color3  = '#FBB829',
    color4  = '#2C78BF',
    color5  = '#FF5C8F',
    color6  = '#0AAEB3',
    color7  = '#918175',
    color8  = '#2D2C29',
    color9  = '#F75341',
}

conky.text = [[

${alignc}-- MPD --
${color2}${mpd_artist}${color}
${color7}${mpd_title}${color}
${color}${hr 2}
${alignc}-- SYSTEM --

${color6}Datetime: ${alignr}${color3}${time %Y.%m.%d %H:%M:%S}${color}
${color6}Kernel: $alignr${color3}$kernel${color}
${color6}Uptime: $alignr${color3}$uptime${color}
${color6}Temperature: ${alignr}${color3}${exec sensors | grep Package | cut -c17-20 | sed '/^$/d'}C${color}

${color6}Processes ${alignr}${color3}$processes ($running_processes running)${color}

${alignc}-- Cpu, memory and disk IO --
${color}CPU: ${color3}@ ${color1}${freq_g cpu}${color} GHz${color1}${cpugraph 10,  color1  FFFFFF}${color}

Core1 » ${cpubar cpu1 10,90} ${color1} ${cpu cpu1}${color} %${goto 200} Temp ${color1}${execi 6 /usr/bin/sensors | grep 'Core 0' | awk '{print $3}' | cut -c2-3}${color} °C
Core2 » ${cpubar cpu2 10,90} ${color1} ${cpu cpu2}${color} %${goto 200} Temp ${color1}${execi 6 /usr/bin/sensors | grep 'Core 1' | awk '{print $3}' | cut -c2-3}${color} °C

${color}RAM$color1${memgraph 10, 000000,ffffff}
$color1$mem$color of${color1} $memmax${color} ${membar 8, 70} $color3${memperc}$color%
${color}Processes ${processes}, Running ${running_processes}

${color}DISK $color3|$color ${fs_bar 10,40}$color1  ${fs_used_perc}$color0 %$color1 ${color0}$color3|$color Temp$color3 ${exec sudo nvme smart-log /dev/nvme0 | grep '^temperature' | cut -c39-40}$color °C ${hr 2}
Read: $color1${diskio_read} ${alignr}${goto 170} ${color}Write:$color1 ${diskio_write}
${diskiograph_read /dev/nvme0n1 15,100 000000 ffffff, -l  -t }${alignr}${diskiograph_write /dev/nvme0n1 15,100 000000 ffffff, -l  -t }
${color6}/${goto 90}$color2${fs_used /} $color1 of $color2${fs_size /}
${color6}/home${goto 90}$color2${fs_used /home/gagbo} $color1  of $color2${fs_size /home/gagbo} ${color}

${alignc}-- Network (Essid: $color3${wireless_essid wlp4s0}$color) --
Local IP : ${addrs wlp4s0}
Public IP : ${exec wget http://ipinfo.io/ip -qO -} ${if_existing /proc/net/route wlp4s0}
${color}Quality: $color3${wireless_link_qual wlp4s0}%
${color}Down: $color1${downspeed wlp4s0} ${alignr}${goto 195}${color}Up: ${color1}${upspeed wlp4s0}
${downspeedgraph wlp4s0 20,140 000000 ffffff} ${alignr}${upspeedgraph wlp4s0
20,140 000000 ffffff}$color
Total: ${totaldown wlp4s0} ${alignr}Total: ${totalup wlp4s0} ${endif} ${if_existing /proc/net/route eth0}
${color}Down: $color1${downspeed eth0} ${alignr}${goto 195}${color}Up: ${color1}${upspeed eth0}
${downspeedgraph eth0 20,140 000000 ffffff} ${alignr}${upspeedgraph eth0
20,140 000000 ffffff}$color
Total: ${totaldown eth0} ${alignr}Total: ${totalup eth0} ${endif}${color}

${alignc}-- Processes --
${goto 40}${color6}Proc${color}${alignr 39}${color6}${color6}Mem${color}
${goto 40}${color1}${top_mem name 1}${color}${alignr 39}${top_mem mem_res 1}
${goto 40}${color1}${top_mem name 2}${color}${alignr 39}${top_mem mem_res 2}
${goto 40}${color1}${top_mem name 3}${color}${alignr 39}${top_mem mem_res 3}

${goto 40}${color6}Proc${color}${alignr 39}${color6}Cpu %${color}
${goto 40}${color1}${top_mem name 1}${color}${alignr 39}${top cpu 1} %
${goto 40}${color1}${top_mem name 2}${color}${alignr 39}${top cpu 2} %
${goto 40}${color1}${top_mem name 3}${color}${alignr 39}${top cpu 3} %
${color}${hr 2}
${alignc}-- Batteries --

Battery 0: ${battery_bar BAT0} ${battery_percent BAT0}%
Battery 1: ${battery_bar BAT1} ${battery_percent BAT1}%
AC power: $color3${acpiacadapter}
]]

