/**
 * ROFI Custom Color theme
 * Red and Dark theme ;)
 * by kingmk13 from modifing a preset available.
 */

* {
    selected-normal-foreground:  rgba ( 250, 130, 0, 100 % ); // premier plan selectioné FAIT
    foreground:                  rgba ( 219, 8, 23, 100 % ); // premier plan FAIT
    normal-foreground:           @foreground;
    alternate-normal-background: rgba ( 0, 0, 0, 0 % );
    red:                         rgba ( 220, 50, 47, 100 % );
    selected-urgent-foreground:  @selected-normal-foreground; //toujours celles inactives du Workspace1
    blue:                        rgba ( 38, 139, 210, 100 % );
    urgent-foreground:           @normal-foreground;  //toujours celles inactives du Workspace1
    alternate-urgent-background: rgba ( 0, 0, 0, 0 % );
    active-foreground:           rgba ( 0, 100, 225, 100 % ); // fenetre active non selectionné FAIT
    lightbg:                     rgba ( 238, 232, 213, 100 % );
    selected-active-foreground:  rgba ( 15, 170, 240, 100 % ); // fenetre active  selectionné FAIT
    alternate-active-background: rgba ( 0, 0, 0, 0 % );
    background:                  rgba ( 15, 16, 28, 80 % ); // arrere plan FAIT
    bordercolor:                 rgba ( 219, 23, 58, 100 % );
    alternate-normal-foreground: @foreground;
    normal-background:           rgba ( 0, 0, 208, 0 % ); // ???
    lightfg:                     rgba ( 88, 104, 117, 100 % );
    selected-normal-background:  rgba ( 35, 35, 85, 33 % ); // arriere plan selcione FAIT
    border-color:                @foreground;
    spacing:                     2;
    separatorcolor:              rgba ( 235, 125, 8, 100 % ); // Separateur 
    urgent-background:           rgba ( 0, 0, 208, 0 % ); // ???
    selected-urgent-background:  @selected-normal-background; //toujours celles inactives du Workspace1
    alternate-urgent-foreground: @urgent-foreground;
    background-color:            rgba ( 0, 0, 0, 0 % );
    alternate-active-foreground: @active-foreground;
    active-background:           rgba ( 0, 0, 28, 0 % );  // ???
    selected-active-background:  rgba ( 35, 35, 85, 33 % );  // fenetre active selectionné FAIT

    prompt-foreground:           rgba ( 125, 55, 185, 100 % ); // super
    entry-foreground:            rgba ( 85, 45, 255, 100 % ); // cool
}
#window {
    background-color: @background;
    location:  center;
    width:  80%;
    border:           0;
    padding:   2% 1% 3%;
    fullscreen:    false;
    font: "Ubuntu Nerd Font 14";
}
#mainbox {
    border:  0;
    padding: 0;
}
#message {
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
    padding:      1px ;
}
#textbox {
    padding:      24px 1em 1em 24px;
    border-color: @separatorcolor;
    text-color: @selected-active-foreground;
    background-color: @selected-active-background;
    font: "Zekton 14";
    columns: 4;
}
#listview {
    fixed-height: 0;
    lines: 10;
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
    spacing:      4px ;
    scrollbar:    true;
    padding:      2% 1% 0% ;
    columns:      6;
}
#element {
    border:  0;
    padding: 1px ;
}
#element.normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
#element.normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
#element.normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
#element.selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
    border:  0 0 1 0;
    border-color:     @selected-active-foreground;
}
#element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
    border:  0;
    border-color:     @selected-active-foreground;
}
#element.selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
    border:  0;
    border-color:     @selected-active-foreground;
}
#element.alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
#element.alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
#element.alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
#scrollbar {
    background-color: @selected-active-background;
    handle-color: @selected-active-foreground;
    border:       0px;
    handle-width: 6px ;
    padding:      0;
}
#mode-switcher {
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
}
#button.selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
#inputbar {
    spacing:    0;
    text-color: @normal-foreground;
    padding:    1px ;
}
#case-indicator {
    spacing:    0;
    text-color: @normal-foreground;
}
#entry {
    spacing:    0;
    text-color: @entry-foreground;
    font: "Zekton 18";
}
#prompt, button{
    spacing:    0;
    text-color: @prompt-foreground;
    font: "Zekton 18";
}
#inputbar {
    children:   [ prompt,textbox-prompt-colon,entry,case-indicator ];
}
#textbox-prompt-colon {
    expand:     false;
    str:        " :";
    margin:     0px 0.3em 0em 0em ;
    text-color: @normal-foreground;
    font: "Zekton 18";
}

/* vim: set filetype=css: */
