#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Vi mode in bash, enter normal mode with [esc]
# set -o vi

SEED1=1
SEED2=03
FREQ1=0.2
FREQ2=0.24
figlet -f slant Archlinux | lolcat -S $SEED1 -F $FREQ1
echo -e "Bienvenue $(whoami)! Nous sommes le $(date +"%A %d %B %Y").\n" | lolcat -S $SEED2 -F $FREQ2


alias ls='ls --color=auto'
alias lc='ls -alh --group-directories-first -T 12 --color=yes'
alias tt='tree -L 2 -aFCh'
alias nn='neofetch | lolcat -a -d 3 -s 100 -F 0.16 -S 1'
alias grep='grep --color=auto'
alias diff='diff --color=always'
alias less='less -Q'
alias glxgears='vblank_mode=0 glxgears'
#alias i3-lock="i3lock && echo mem > /sys/power/state"

# To see bash colors, tap : for x in {0..8}; do for i in {30..37}; do for a in {40..47}; do echo -ne "\e[$x;$i;$a""m\\\e[$x;$i;$a""m\e[0;37;40m "; done; echo; done; done; echo ""

# --~COLORED MAN PAGES~--
man(){
    LESS_TERMCAP_md=$'\e[01;31m'\
    LESS_TERMCAP_me=$'\e[0m'\
    LESS_TERMCAP_se=$'\e[0m'\
    LESS_TERMCAP_so=$'\e[01;44;33m'\
    LESS_TERMCAP_ue=$'\e[0m'\
    LESS_TERMCAP_us=$'\e[01;32m'\
    command man "$@"
}


# PS1="\[\e[0;31m\]\H \[\e[0;34m\]\W\[\e[m\] $ "
PS1='[\u@\h \W]\$ '
# PS1='[\e]2;XTerm\a \u \W]\$ '

#Powerline
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/share/powerline/bindings/bash/powerline.sh




# Export XDG base directory for user only (see .config/environment.d/ for systemd exported PATH)
XDG_CONFIG_HOME="$HOME/.config"
XDG_CACHE_HOME="$HOME/.cache"
XDG_DATA_HOME="$HOME/.local/share"



export HISTTIMEFORMAT="%d/%m/%y %T "
export HISTSIZE=10000
export HISTFILESIZE=10000

export BROWSER=firefox
export EDITOR="vim"
export TERMINAL="xterm"
export NUMBA_COLOR_SCHEME="dark_bg"
# export NUMBA_DISABLE_PERFORMANCE_WARNINGS=1

# Navigate and search throught packages with fzf !
# pacman -Qqe | fzf --preview 'pacman -Qi {}'

# Resize /tmp for some compilation work
# mount -o remount,size=12G,noatime /tmp

# Clear inodes, dentries and pagecache :
# sync; echo 3 > /proc/sys/vm/drop_caches


# mounting NTFS partitions :
# sudo ntfs-3g /dev/sda5 /mnt/StageFramatome2020
# sudo ntfs-3g /dev/sda2 /mnt/windows

