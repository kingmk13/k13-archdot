#!/bin/sh


fg='#cf0019ff' # foreground
bg='#33224466' # background
ls='#000000ff' # line & sep
r0='#2266ccff' # ring norma
r1='#55aa11ff' # ring verif
b1='#99770066' # bkgd verif
t1='#338822ff' # text verif
r2='#ff5522ff' # ring wrong
b2='#bb662299' # bckg wrong
t2='#700022ff' # text wrong
kh='#11c0ddff' # key press
bs='#6600ccff' # backspace



B='#00000000'  # blank
C='#ffffff22'  # clear ish
D='#ff00ffcc'  # default
T='#ee00eeee'  # text
W='#880000bb'  # wrong
V='#bb00bbbb'  # verifying

A='#115588ff'  # testing colors

exec i3lock \
--insidevercolor=$b1  \
--ringvercolor=$r1    \
\
--insidewrongcolor=$b2 \
--ringwrongcolor=$r2  \
\
--insidecolor=$bg     \
--ringcolor=$r0       \
--linecolor=$ls       \
--separatorcolor=$ls  \
\
--verifcolor=$t1       \
--wrongcolor=$t2       \
--timecolor=$fg        \
--datecolor=$fg        \
--layoutcolor=$fg      \
--keyhlcolor=$kh      \
--bshlcolor=$bs       \
\
--radius=130          \
--ring-width=10.0     \
\
-f                    \
-t                    \
--screen 1            \
--blur 5              \
--clock               \
--indicator           \
--timestr="%k:%M:%S"  \
--datestr="Le %A %e %B %Y" \
--keylayout 0         \
\
--wrongtext="☹  Échec ! "        \
--veriftext=" Vérification..."  \
--noinputtext="  Vide  "       \
\
--timesize=48        \
--datesize=16        \
--layoutsize=16      \
--verifsize=28       \
--wrongsize=28       \
--time-font=Zekton   \
--date-font=B612     \
--layout-font=B612   \
--verif-font=B612    \
--wrong-font=B612    \
