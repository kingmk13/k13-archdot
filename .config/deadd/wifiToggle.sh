#!/bin/bash

# Enable or disable the wifi ~

STATE=$(nmcli radio wifi)


case $STATE in
    enabled )
        nmcli radio wifi off
        notify-send.py a --hint boolean:deadd-notification-center:true int:id:0 boolean:state:false;;
    disabled )
        nmcli radio wifi on
        notify-send.py a --hint boolean:deadd-notification-center:true int:id:0 boolean:state:true;;
esac

