#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export HISTTIMEFORMAT="%d/%m/%y %T "
