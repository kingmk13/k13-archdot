#!/bin/bash

# Set the good state of Wifi button at startup
state_Wifi=$(nmcli radio wifi)
case $state_Wifi in
     enabled)      
          notify-send.py a --hint boolean:deadd-notification-center:true int:id:0 boolean:state:true;;
     disabled)      
          notify-send.py a --hint boolean:deadd-notification-center:true int:id:0 boolean:state:false;;
esac

sleep 1
notify-send.py a --hint boolean:deadd-notification-center:true int:id:1 boolean:state:true;

sleep 1
notify-send.py a --hint boolean:deadd-notification-center:true int:id:2 boolean:state:false;
