#!/bin/bash

sleep 0
notify-send 'Hello world!' '<u>This is an example notification.</u>' --icon=dialog-information

sleep 1
notify-send 'Notification : ' 'This is <b>an example</b> notification.' --icon=dialog-information

sleep 1
notify-send 'Hello world!' 'This is an example notification.\n\tGood site here : <a href="https://example.com">Link to <b>Website</b></a>' --icon=dialog-information

sleep 1
notify-send 'Hello world!' '<i>This is an example notification.</i>' --icon=dialog-information

sleep 1
notify-send 'Hello world!' 'This is an example <b><i>notification</i></b>.' --icon=dialog-information

