" TODO:
"   Add YCM (ultisnips complete), or replace ALE with CoC (see also coc-snipets)


if has('python3') " Force use of python3, because it was first called ??? https://robertbasic.com/blog/force-python-version-in-vim/
endif
set nocompatible
filetype plugin indent on
"--------------------------------------------------
"------------------ PLUG SETTING ------------------
"--------------------------------------------------
let g:plug_window = "vertical botright new"
call plug#begin('~/.vim/plugged')
" Testing...
Plug 'vim/killersheep' 
" Interface
Plug 'mhinz/vim-startify' 
Plug 'vim-airline/vim-airline' 
Plug 'liuchengxu/vista.vim' " ALE as LSP source doesnt work
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'ryanoasis/vim-devicons'
" Assistance
Plug 'christoomey/vim-system-copy'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'Yggdroot/indentLine'
Plug 'jiangmiao/auto-pairs' 
Plug 'tpope/vim-unimpaired'
Plug 'junegunn/fzf' 
Plug 'junegunn/fzf.vim'
" Coding / Documentation
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround' 
Plug 'tpope/vim-repeat'
Plug 'kana/vim-textobj-user'
" Plug 'kana/vim-textobj-indent'
" Plug 'kana/vim-textobj-entire'
" --> https://github.com/kana/vim-textobj-user/wiki
Plug 'dyng/ctrlsf.vim' 
Plug 'terryma/vim-multiple-cursors' 
Plug 'AndrewRadev/splitjoin.vim' 
Plug 'junegunn/vim-easy-align' 
Plug 'christoomey/vim-sort-motion'
Plug 'kkoomen/vim-doge'
" Fixers, Linters, and Completers
Plug 'dense-analysis/ale' " ccls + vim-ccls + ale => it is with clang... I want gcc.
Plug 'davidhalter/jedi-vim'
Plug 'xavierd/clang_complete', { 'for': ['c', 'cpp'] }
Plug 'SirVer/ultisnips' 
Plug 'honza/vim-snippets' " contributing ?
" Syntax / tags
Plug 'szw/vim-tags'
" Plug 'sheerun/vim-polyglot'
Plug 'dpelle/vim-Grammalecte'
" Git and others CVS
Plug 'mhinz/vim-signify' 
Plug 'tpope/vim-fugitive'
" LaTeX
" Plug 'vim-latex/vim-latex', { 'for': 'tex' }
" Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }

Plug 'lervag/vimtex', { 'for': [ 'tex', 'bib' ] }
" Performance
Plug 'tpope/vim-dispatch' 
call plug#end()



"--------------------------------------------------
"---------------- PLUGINS SETTING -----------------
"--------------------------------------------------


"  |---------------> INTERFACE <-------------------

" Vim-Startify
let g:startify_padding_left = 48
let g:startify_custom_indices = ['', '', '', '', '', '', '', '', '', '']
let g:ascii = [
          \ '   :::     ::::::::::::::  :::   :::        ::::::::',
          \ '  :+:     :+:    :+:     :+:+: :+:+:      :+:    :+:',
          \ ' +:+     +:+    +:+    +:+ +:+:+ +:+     +:+    +:+ ',
          \ '+#+     +:+    +#+    +#+  +:+  +#+      +#++:++#   ',
          \ '+#+   +#+     +#+    +#+       +#+     +#+    +#+   ',
          \ '#+#+#+#      #+#    #+#       #+#     #+#    #+#    ',
          \ ' ###    ##############       ###      ########      ',
          \ '                                                    ',
          \]
let g:startify_custom_header = 'startify#pad(g:ascii + startify#fortune#boxed())'

" Vim-airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#bufferline#enabled = 1
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''

" Vista.vim
let g:vista_icon_indent = ["╰─▸ ", "├─▸ "]
let g:vista_fold_toggle_icons = ['▼', '▶']
" autocmd FileType vista,vista_kind nnoremap <buffer> <silent> \/ :<c-u>call vista#finder#fzf#Run()<CR>
" let g:vista_default_executive = 'ale'
" let g:vista_executive_for = {
"   \ 'c': 'ctags',
"   \ 'cpp': 'ctags',
"   \ 'python': 'ale',
"   \ }
let g:vista_ctags_cmd = {
      \ 'haskell': 'hasktags -x -o - -c',
      \ 'cpp': 'ctags -R --sort=yes --c++-kinds=+pl --fields=+iaS --extras=+q .',
      \ }
let g:vista_fzf_preview = ['up:50%']
let g:vista_keep_fzf_colors = 1
let g:vista_finder_alternative_executives = 'ctags'

" NERDTree settings
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | Startify | NERDTree | wincmd p | endif
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let g:NERDTreeDirArrowExpandable = '―⎜'
let g:NERDTreeDirArrowCollapsible = '―⎝'
let g:NERDTreeHighlightCursorline = 0

" NerdTree syntax Setings
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1
let g:NERDTreeHighlightFolders = 1 " enables folder icon highlighting using exact match
let g:NERDTreeHighlightFoldersFullName = 1 " highlights the folder name

" WebDevicons
set encoding=UTF-8
let g:WebDevIconsUnicodeDecorateFolderNodesDefaultSymbol = ''
let g:DevIconsDefaultFolderOpenSymbol = ''
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols = {}
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['html'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['js'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['md'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['vim'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['yaml'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['yml'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['pro'] = '郎'
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols = {}
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols['.*vimrc.*'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols['.gitignore'] = ''


"  |--------------> ASSISTANCE <-------------------

" Vim system-copy
let g:system_copy#copy_command='xclip -sel clipboard'
let g:system_copy#paste_command='xclip -sel primary -o'

" IndentLine
let g:indentLine_color_term = 8
let g:indentLine_char = '┇'
let g:indentLine_concealcursor = 'n'
let g:indentLine_conceallevel = 2
let g:indentLine_setConceal = 0
let g:indentLine_fileTypeExclude = ['text', 'startify', 'help', 'tex']

" FZF for vim setings, and fzf.vim preconfigured's FZF commands
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }
let g:fzf_layout = { 'left': '~33%' }
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
let g:fzf_buffers_jump = 1
command! -bang Colors
  \ call fzf#vim#colors({'left': '12%', 'options': '--reverse --margin 8%,1'}, <bang>0)


"  |----------> CODING / DOCUMENTATION <-----------

" CtrlSF to search pattern in files
let g:ctrlsf_default_root = 'cwd'
let g:ctrlsf_context = '-B 3 -A 5'
let g:ctrlsf_default_view_mode = 'normal'
let g:ctrlsf_search_mode = 'async'
let g:ctrlsf_position = 'left'
let g:ctrlsf_winsize = '33%'

" Vim-Easy-Align Setings
let g:easy_align_interactive_modes = ['l', 'c', 'r']
let g:easy_align_bang_interactive_modes = ['r', 'c', 'l']
let g:easy_align_bypass_fold = 1

" Vim-DoGe Setings
let g:doge_mapping = '<Leader>dg'
let g:doge_doc_standard_python = 'numpy'


"  |------> FIXERS, LINTERS & COMPLETERS <---------

" ALE Settings
let g:ale_sign_info = ''
let g:ale_sign_error = ''
let g:ale_sign_warning = ''
let g:ale_set_highlights = 1
autocmd VimEnter * :let g:ale_change_sign_column_color = 1
autocmd VimEnter * :let g:ale_sign_column_always = 1
autocmd VimEnter * :highlight! ALESignColumnWithErrors ctermfg=0 ctermbg=0 guifg=#A5A5A5 guibg=#F5F5F5
autocmd VimEnter * :highlight! ALESignColumnWithoutErrors ctermfg=0 ctermbg=8 guifg=#A5A5A5 guibg=#F5F5F5
autocmd VimEnter * :highlight! ALEErrorSign ctermfg=9 ctermbg=8 guifg=#C30500 guibg=#F5F5F5
autocmd VimEnter * :highlight! ALEWarningSign ctermfg=11 ctermbg=8 guifg=#ED6237 guibg=#F5F5F5
autocmd VimEnter * :highlight! ALEInfoSign   ctermfg=14 ctermbg=8 guifg=#ED6237 guibg=#F5F5F5
autocmd VimEnter * :highlight! ALEError ctermfg=9 ctermbg=8 guifg=#C30500 guibg=#F5F5F5
autocmd VimEnter * :highlight! ALEWarning ctermfg=11 ctermbg=8 guifg=#ED6237 guibg=#F5F5F5
autocmd VimEnter * :highlight! ALEInfo   ctermfg=14 ctermbg=8 guifg=#ED6237 guibg=#F5F5F5
let g:airline#extensions#ale#error_symbol = 1
let g:airline#extensions#ale#warning_symbol = 1
let g:ale_cache_executable_check_failures = 1
let g:ale_maximum_file_size = 4194304

autocmd FileType  ['c', 'cpp', 'python', 'tex'] let g:ale_linters_explicit = 1
let g:ale_set_loclist = 1
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'bash': ['remove_trailing_lines', 'trim_whitespace', 'eslint', 'shfmt'],
\   'c': ['remove_trailing_lines', 'trim_whitespace', 'uncrustify'],
\   'cpp': ['remove_trailing_lines', 'trim_whitespace', 'uncrustify'],
\   'python': ['remove_trailing_lines', 'trim_whitespace', 'isort', 'autopep8'],
\   'javascript': ['remove_trailing_lines', 'trim_whitespace', 'eslint'],
\}
let g:ale_python_pylint_options = '--jobs 8 --variable-rgx "[a-zA-Z0-9_]" --const-rgx "[a-zA-Z0-9_]"'
" let g:ale_python_pylint_options = '--jobs 8 --max-line-length 119'
let g:ale_linters = {
\   'c': ['gcc', 'cppcheck', 'cpplint', 'flawfinder', 'ccls'],
\   'cpp': ['gcc', 'clazy', 'cppcheck', 'cpplint', 'flawfinder', 'ccls'],
\   'python': ['pylint'],
\   'tex': ['texlab'],
\}
let g:ale_list_window_size = 8
" let g:ale_completion_enabled = 1
" set omnifunc=ale#completion#OmniFunc
let g:ale_set_balloons = 1

" Vim-Jedi
let g:jedi#use_splits_not_buffers = "bottom"

" Clang-Complete Setings
 " path to directory where library can be found
 let g:clang_library_path='/usr/lib/libclang.so'
 " Create a ".clang_complete" file in project directory to specify option !

" UltiSnips Setings
" let g:pymode_python = 'python3'
let g:UltiSnipsEditSplit="vertical"
" let g:UltiSnipsSnippetDirectories=["UltiSnips", "mycoolsnippets"]


"  |--------------> SYNTAX / TAGS <----------------

" Vim-tags Setings
let g:vim_tags_auto_generate = 0
" let g:vim_tags_project_tags_command = "{CTAGS} -R {OPTIONS} {DIRECTORY} 2>/dev/null"
let g:vim_tags_use_vim_dispatch = 1
let g:vim_tags_use_language_field = 1
let g:vim_tags_main_file = 'vimtags'
let g:vim_tags_cache_dir = expand("%:p:h") 

" Tags Settings
" set tags=./tags;,tags;
autocmd FileType  c set tags+=~/.vim/tags/sdl
autocmd FileType cpp set tags+=~/.vim/tags/cpp,~/.vim/tags/qt
set tags+=vimtags
" set cpt=.,w,b,u,t,i

" Polyglot Setings
let g:polyglot_disabled = ['latex']

" Grammalecte settings
let g:grammalecte_cli_py='~/.config/Grammalecte/grammalecte-cli.py'
hi GrammalecteGrammarError  guisp=blue gui=undercurl guifg=NONE guibg=NONE ctermfg=4 ctermbg=none term=underline cterm=none
hi GrammalecteSpellingError guisp=red  gui=undercurl guifg=NONE guibg=NONE ctermfg=1 ctermbg=none  term=underline cterm=none
let g:grammalecte_win_height = 16

" OmniCpp settings
set omnifunc=syntaxcomplete#Complete
" let OmniCpp_NamespaceSearch = 2
" let OmniCpp_GlobalScopeSearch = 1
" let OmniCpp_ShowAccess = 1
let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
let OmniCpp_MayCompleteDot = 1 " autocomplete after .
let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]
let OmniCpp_SelectFirstItem = 2 " select first item (but don't insert)
let OmniCpp_ShowPrototypeInAbbr = 1 " show function prototype (i.e. parameters) in popup window
" autocmd CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif    " autoclose
set complete=.,w,b,u,t,i,kspell]
set completeopt=menuone,menu,longest,preview


"  |---------------> CVS and GIT <-----------------

" Signify
set updatetime=100


"  |-----------------> LaTeX <---------------------

" LaTeX-Suite
" let g:tex_flavor='latex'
" set iskeyword+=:

" Vim-Tex
let g:tex_flavor='latex'
let g:vimtex_complete_enabled = 1
let g:vimtex_complete_close_braces = 1
let g:vimtex_view_general_viewer = 'zathura'
let g:Tex_GotoError = 1
let g:Tex_ShowErrorContext = 1

" LaTeX-Live-Preview
let g:livepreview_previewer = 'zathura'
" let g:livepreview_engine = 'your_engine' . ' [options]'
let g:livepreview_cursorhold_recompile = 0



"--------------------------------------------------
"---------------- GENERAL SETTINGS ----------------
"--------------------------------------------------

" Set the file's dir as the current dir
set autochdir
" autocmd BufEnter * silent! lcd %:p:h
" Enabling plugins and indent
filetype plugin indent on
" Enabling syntax highlighting
syntax on
set synmaxcol=512
set textwidth=120
autocmd FileType text,tex set textwidth=0
set linebreak
set breakindent
set breakindentopt=shift:6
autocmd FileType text set nobreakindent
set autoindent   " indent when moving to the next line while writing code  => only works in Cpp, interfere with others programs
set expandtab    " expand tabs into spaces
set ts=4         " set tabs to have 4 spaces
set shiftwidth=4 " when using the >> or << commands, shift lines by 4 spaces
set mouse=a      " Use mouse to move cursor
set cursorline   " show a visual line under the cursor's current line
set whichwrap=b,s,<,>,[,]   " Traverse line
set backspace=indent,eol,start
set foldmethod=indent   " Work fine ('syntax' kill simply perf !)
set foldlevel=20    " Default fold level
set number       " Set lines number
set relativenumber
set showmatch    " show the matching part of the pair for [] {} and ()
set laststatus=2 " display file name on the bottom bar
" set lazyredraw   " the screen will not be redrawn while executing macros, registers and other commands that have not been typed.  Also, updating the window title is postponed.
" set ttyfast      " Improves smoothness of redrawing when there are multiple windows and the terminal does not support a scrolling region.
" set re=1         " Use old regexp engine, 2 is for NFA (slow there) and 0 for automatic choice
" set grepprg=grep\ -nH\ $*
let python_highlight_all = 1 " enable all Python syntax highlighting features



"--------------------------------------------------
"-------------------- MAPPINGS --------------------
"--------------------------------------------------

" Remap the leader key
let mapleader = ","
let maplocalleader = '²'

" General
nnoremap <C-F12> :!ctags -R --sort=yes --c++-kinds=+pl --fields=+iaS --extras=+q .<CR>
nnoremap <F12> :cd %:p:h <bar> TagsGenerate!<CR>
imap <C-F11> <Plug>(ale_complete)
nmap <F10> <Plug>(ale_fix)
nnoremap <silent> <C-F9> :Vista finder fzf<CR>
nnoremap <silent> <F9> :Vista!!<CR>
nnoremap <C-F8> :silent ! ranger<CR>
nnoremap <F8> :NERDTreeToggle<CR>
nnoremap <C-F5> :LLPStartPreview<CR>
nmap <F5> <Esc>:w<CR>:!clear;python %<CR>
vmap <F5> <Esc>:w<CR>:!clear;python %<CR>
imap <F5> <Esc>:w<CR>:!clear;python %<CR>
noremap <C-P> @:
nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>
nnoremap <Leader>l :set relativenumber!<CR>
set pastetoggle=<F3> "paste mode toggling

" Ctrl + C to xclip
vnoremap <C-c> "+y

" CtrlSF shortcuts
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>

" Vim-Easy-Align shortcuts
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" ALE maps
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" Alternate (a.vim)
nnoremap <Leader>a :A <CR>
nnoremap <Leader>as :AS <CR>
nnoremap <Leader>at :AT <CR>
nnoremap <Leader>av :AV <CR>

" Allow us to use Ctrl-s and Ctrl-q as keybinds
silent !stty -ixon
" Restore default behaviour when leaving Vim.
autocmd VimLeave * silent !stty ixon
" Quit vim
nnoremap <C-Q> :q<CR>
vnoremap <C-Q> <esc>:q<CR>
inoremap <C-Q> <esc>:q<CR>
" Force quit vim
nnoremap <C-S-Q> :q!<CR>
vnoremap <C-S-Q> <esc>:q!<CR>
inoremap <C-S-Q> <esc>:q!<CR>
" Save changes
nnoremap <C-S> :w<CR>
vnoremap <C-S> <esc>:w<CR>
inoremap <C-S> <esc>:w<CR>
" Force sudo Save changes
nnoremap <C-S-S> :!sudo -s % <CR> :w !sudo tee % /dev/null<CR>
vnoremap <C-S-S> <esc>:w !sudo tee %<CR>
inoremap <C-S-S> <esc>:w !sudo tee %<CR>



"--------------------------------------------------
"--------------------- THEMING --------------------
"--------------------------------------------------

set guifont=Hack\ Nerd\ Font\ Mono\ 12
colo default   " set colorscheme
set t_Co=256      " define number of colors of terminal
highlight Comment cterm=italic gui=italic
highlight Function cterm=bold gui=italic
" Autocmd at VimEnter, bc indentline change highlight group Conceal before
autocmd VimEnter *.tex  :hi Conceal guisp=red gui=undercurl guifg=NONE guibg=NONE ctermfg=1 ctermbg=none term=underline cterm=none
hi Conceal guisp=red gui=undercurl guifg=NONE guibg=NONE ctermfg=1 ctermbg=none term=underline cterm=none
au FileType tex set cole=1
hi CursorLine   cterm=NONE ctermbg=0 ctermfg=none guibg=grey guifg=white
hi CursorColumn cterm=NONE ctermbg=0 ctermfg=none guibg=grey guifg=white

" Set the speller for .txt and .tex files (installed for english or french, vim-spell-{en,fr})
"   Set default spell language to French (use spellinfo to view curently installed)
autocmd FileType text setlocal spell spelllang=fr,en_us
autocmd FileType tex setlocal spell spelllang=fr,en_us
hi clear SpellBad
hi clear SpellCap
hi clear SpellLocal
hi clear SpellRare
hi SpellBad guisp=red gui=undercurl guifg=NONE guibg=NONE ctermfg=1 ctermbg=none term=underline cterm=none
hi SpellCap guisp=yellow gui=undercurl guifg=NONE guibg=NONE ctermfg=3 ctermbg=none term=underline cterm=none
hi SpellLocal guisp=cyan gui=undercurl guifg=NONE guibg=NONE ctermfg=6 ctermbg=none term=underline cterm=none
hi SpellRare guisp=magenta gui=undercurl guifg=NONE guibg=NONE ctermfg=5 ctermbg=none term=underline cterm=none

" Highlight overlength lines
highlight OverLength ctermbg=7 ctermfg=red guibg=#592929
" match OverLength /\%121v.\+/

" Highlight popup menu
hi Pmenu gui=NONE guibg=NONE cterm=none ctermbg=153

" Highlight Todo
hi Todo gui=NONE guibg=NONE cterm=bold ctermfg=1

" Open help on left with good size
autocmd! FileType help wincmd L | vertical resize 80

" Remember position of last edit and return on reopen
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
